# README #

NilFactorDatabase is a php class which is designed to allow you to quickly create php class models to extend NilFactorDatabase and get your code moving.
This code is Open Source and you are free to use it at your own risk.

### What is this repository for? ###

One class that you can use to quickly create database models and access/insert data into a database.

### How do I get set up? ###

In your project where the NilFactorDatabase.php sits you'll need a folder called configs and a file in that folder called db_config.ini.
You will need to edit this file to have your database settings in it. This currently has been tested and worked with Postgres and MySQL.


### Contribution guidelines ###

If you have made a change and would like to share it, create a pull request and I'll look at it.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact