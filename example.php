<?php
// load the classes (you would probably want to use autoload or w/e to do this not in each class)
require_once("NilfactorDatabase.php");
require_once("ExampleUser.php");

// Create the User Object
$User = new ExampleUser();

$User->name = "Test User";
$User->username = "tuser";
$User->email = "tuser@tuser.com";
$User->password = "abc123";

// Store the user object
$User->storeRecord();

// it should have a user_id;
echo "User Record Saved \n". print_r($User, true);

// Update the record
$User->name = "Some User";
//$User->password = "abc@123";

// Update the user object
$User->storeRecord();

// it should have the same user_id, hash for password should be different;
echo "User Record updated \n". print_r($User, true);

// lookup by login
$UserLookup = ExampleUser::queryLogin("tuser");
echo "This record should have been found by username and be the same as the last record \n" .print_r($UserLookup, true);

// lookup by user_id
$UserByID = ExampleUser::queryByID($User->user_id);
echo "This record should have been found by user_id and be the same as the last record \n" .print_r($UserByID, true);

// cleanup user
$User->deleteRecord();
$UserShouldBeEmpty = ExampleUser::queryByID($User->user_id);
echo "The user should now be deleted \n".print_r($UserShouldBeEmpty, true);