<?php
    class ExampleUser extends NilFactorDatabase {
        protected static $table_name = "example_user";
        protected static $primary_key = "user_id";
        protected static $creation_field = "created_on";
        protected static $update_field = "last_login";

        // for postgresql only (needed to get the sequence last id)
        protected static $sequence_name = "example_user_user_id_seq";

        protected static $fields = [
            'user_id'    => "num",
            'username'   => "string",
            'email'      => "string",
            'password'   => "string",
            'name'       => "string",
            'created_on' => "timestamp_w_z",
            'last_login' => "timestamp_w_z",
        ];

        protected $encrypted_fields = array('password');

        public $user_id;
        public $username;
        public $email;
        public $password;
        public $name;
        public $created_on;
        public $last_login;

        public static function getTableName() {
            return self::$table_name;
        }

        public static function getKeyField() {
            return self::$key_field;
        }

        public static function getFields() {
            return self::$fields;
        }

        public static function queryLogin($login = null) {
            $params = array('username' => $login);
            $rows = self::queryRecords($params);
            if (!empty($rows)) {
                $user = $rows[0];
                return $user;
            }
            return false;
        }

        public function storeRecord() {
            $user = static::queryLogin($this->username);
            if (!empty($user) && empty($this->user_id)) {
                throw new Exception("This login name is already taken.");
            } else if (empty($this->username)) {
                throw new Exception("ERROR[storeRecord]: User can not store record on blank login");
            } else {
                parent::storeRecord();
            }
        }

        public function verifyPassword($password) {
            if (password_verify($password, $this->password) == $this->password) {
                return true;
            } else {
                return false;
            }
        }
    }
?>