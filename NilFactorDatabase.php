<?php
/**
 * File: NilFactorDatabase.php
 * Written By: Richard Williamson
 */
abstract class NilFactorDatabase {
    // don't need to worry about in extended class
    protected static $instance;
    protected static $database;
    protected static $transaction = false;
    protected $encypt_algo = '$2y$10$'; // may have to change this as php changes default

    // you will need to add in your extended class
    protected static $table_name;
    protected static $primary_key;
    protected static $sequence_name; // for postgresql dbs (not needed for mysql)
    protected $encrypted_fields = []; // Leave empty unless you want a field to always be encrypted
    protected static $creation_field = null;
    protected static $update_field = null;
    protected static $order_by = ''; // order by

    protected static $fields = [
        /**
         * The below is an example for you to use in a child class
         *
         * 'object_id'        => 'num',
         * 'name'             => 'string',
         * 'is_test'          => 'bool',
         * 'update_timestamp' => 'timestamp',
         * 'timestampwzone'   => 'timestamp_w_z'
         * 'creation_date'    => 'date',
         */
    ];

    // add this functionality to your class
    abstract public static function getTableName();
    abstract public static function getFields();

    CONST CONFIG_FILE = './configs/db_config.ini';

    /**
     * @brief on creation..
     * Initialize the database object on creating
     */
    public function __construct() {
        static::initDatabase();
    }

    /**
     * @brief This function allows us to create the PDO object based on a ini file.
     */
    protected static function initDatabase() {
        if (file_exists(static::CONFIG_FILE)) {
            $data = parse_ini_file(static::CONFIG_FILE);
            if (!empty($data['database'])) {
                $db_settings = json_decode($data['database']);
                $conn_string = $db_settings->type.":host=".$db_settings->host.";dbname=".$db_settings->db_name;
                static::$database = new PDO($conn_string, $db_settings->username, $db_settings->password);
            } else {
                throw new Exception("ERROR[__construct]: NilFactorDatabase had no settings available to load.");
            }
        } else {
            throw new Exception("ERROR[__construct]: NilFactorDatabase had no settings available to load.");
        }
    }

    /**
     * @brief get the instance if it's created if not create it
     */
    public static function getInstance() {
        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * @brief Starts a database transaction which will allow us to rollback the database on error
     */
    public static function begin() {
        if (static::$transaction === false) {
            if (empty(static::$database)) {
                static::initDatabase();
            }
            static::$database->beginTransaction();
        }
    }

    /**
     * @brief Rollback a database transaction
     */
    public static function rollback() {
        if (static::$transaction === true) {
            static::$database->rollback();
        }
    }

    /**
     * @brief Commits a database transaction.
     */
    public static function commit() {
        if (static::$transaction === true) {
            static::$database->commit();
        }
        static::$transaction = false;
    }

    /**
     * @brief query the database
     * @param string $sql - sql query string
     * @param array $params - parameters for the query
     * @return stmt
     */
    public static function query($sql, $params = null) {
        if (empty(static::$database)) {
            static::initDatabase();
        }

        $stmt = static::$database->prepare($sql);
        if ($stmt && $stmt->execute($params)) {
            return $stmt;
        } else {
            throw new Exception("ERROR[query]: NilFactorDatabase could not prepare a statement. Please check your PDO Settings or SQL. \n{$sql} \n" .print_r($params, true));
        }
    }

    /**
     * @brief look up record by primary identifier
     * @param integer $id - a string or number value of the key database field we want to lookup
     * @return object
     */
    public static function queryByID($id) {
        $table_name = static::getTableName();
        if (!empty($table_name)) {
            $primary_key = static::$primary_key;
            if (!empty($primary_key)) {
                $sql = "select * from " .$table_name. " where " .$primary_key. " = :id \n";
                $params = ['id' => $id];
                $stmt = static::query($sql, $params);
                $class = get_called_class();
                $stmt->setFetchMode(PDO::FETCH_CLASS, $class);
                $row = $stmt->fetch();
                return $row;
            } else {
                throw new Exception("ERROR[queryByID]: NilFactorDatabase has no key field set. You must extend this in your child class to use this function.");
            }
        } else {
            throw new Exception("ERROR[queryByID]: NilFactorDatabase has no table name set. You must extend this in your child class to use this function.");
        }
    }

    /**
     * @brief with given parameters return all records found with query
     * @param array $search - search paramaters with field_names as key.
     * @return array
     */
    public static function queryRecords($search) {
        $table_name = static::getTableName();
        if (!empty($table_name)) {
            $fields = static::getFields();
            if (!empty($fields)) {
                if (is_array($search)) {
                    $params = [];
                    $sql = "select * from " .$table_name. " "
                         . static::generateWhereStatement($search, $params);

                    if (!empty(static::$order_by)) {
                        $sql .= " order by " .static::$order_by. " ";
                    }

                    $stmt = static::query($sql, $params);
                    $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

                    return $stmt->fetchAll();
                } else {
                    throw new Exception("ERROR[queryRecords]: NilFactorDatabase::queryRecords needs an array given as paramater 1.");
                }
            } else {
                throw new Exception("ERROR[queryRecords]: NilFactorDatabase has not fields set. You must extend this in your child class to use this function.");
            }
        } else {
            throw new Exception("ERROR[queryRecords]: NilFactorDatabase has no table name set. You must extend this in your child class to use this function.");
        }
    }

    /**
     * queryEach()
     * @brief allows us to query large amounts of data without running out of memory
     * @param array $search - search parameters with field_names as key.
     * @param Closure $callback - function to call back
     */
    public static function queryEach($search, Closure $callback) {
        $table_name = static::getTableName();
        if (!empty($table_name)) {
            $fields = static::getFields();
            if (!empty($fields)) {
                if (is_array($search)) {
                    $params = [];
                    $sql = "select * from " .$table_name. " "
                         . static::generateWhereStatement($search, $params);

                    if (!empty(static::$order_by)) {
                        $sql .= " order by " .static::$order_by. " ";
                    }

                    $stmt = static::query($sql, $params);
                    $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

                    while ($row = $stmt->fetch()) {
                        $callback($row);
                    }
                } else {
                    throw new Exception("ERROR[queryEach]: NilFactorDatabase::queryEach needs an array given as paramater 1.");
                }
            } else {
                throw new Exception("ERROR[queryEach]: NilFactorDatabase has not fields set. You must extend this in your child class to use this function.");
            }
        } else {
            throw new Exception("ERROR[queryEach]: NilFactorDatabase has no table name set. You must extend this in your child class to use this function.");
        }
    }

    /**
     * @brief encrypt a string with the password_hash function
     * @param string $string - what you want to hash
     * @return string - return the encrypted string
     */
    protected function encrypt($string) {
        return(password_hash($string, PASSWORD_DEFAULT));
    }

    /**
     * @brief generate the sql for a where statement
     * @param array $search - array search params
     * @param array &$params - the params to pass to query
     * @return string
     */
    protected static function generateWhereStatement(array $search, array &$params) {
        $sql = "";
        $where = [];

        foreach($search as $key => $value) {
            $params[$key] = $value;
            if (strtolower($fields[$key]) != 'string') {
                $where[] = $key. " = :" .$key;
            } else {
                $where[] = $key. " like :" .$key;
            }
        }
        if (!empty($params)) {
            $sql = "where " .implode(" and ", $where);
        }

        return $sql;
    }

    /**
     * @brief Allows us to create a new record or update an existing record.
     *        If we have update/create timestamp we take care of these if it applies to the update or creation.
     *        If we have need to encrypt a field we take care of that here as well.
     *        Based on the field type (string, num, bool...) we will set base values according to it if they are empty
     *        Based on the field type we will set the values accordingly.
     */
    public function storeRecord() {
        $table_name = static::getTableName();
        foreach ($this->encrypted_fields as $field) {
            if (strrpos($this->{$field}, $this->encypt_algo) === false && !empty($this->{$field})) {
                $this->{$field} = $this->encrypt($this->{$field});
            }
        }
        if (!empty($table_name)) {
            $primary_key = static::$primary_key;
            if (!empty($primary_key)) {
                $fields = static::getFields();
                if (!empty($fields)) {
                    $this->updateTimeFields();

                    // default the values
                    $params = [];
                    $keys = [];
                    $values = [];
                    $sets = [];

                    list($keys, $sets, $values, $params) = $this->getKeysAndValues($fields);

                    if (empty($this->{$primary_key})) { // insert
                        $sql = "insert into " .$table_name. "(" .implode(', ', $keys). ") values (" .implode(', ', $values). ") ";
                        $stmt = static::query($sql, $params);
                        $sequence_name = !empty(static::$sequence_name) ? static::$sequence_name : $primary_key;
                        $id = static::$database->lastInsertId($sequence_name);
                        $this->{$primary_key} = $id;
                        return $stmt;
                    } else { // update
                        $params[$primary_key] = $this->{$primary_key};
                        $sql = "update " .$table_name. " set " .implode(', ', $sets). " where " .$primary_key. " = :" .$primary_key. " ";
                        $stmt = static::query($sql, $params);
                        return $stmt;
                    }
                } else {
                    throw new Exception("ERROR[storeRecord]: NilFactorDatabase has fields set. You must extend this in your child class to use this function.");
                }
            } else {
                throw new Exception("ERROR[storeRecord]: NilFactorDatabase has no primary_key set. You must extend this in your child class to use this function.");
            }
        } else {
            throw new Exception("ERROR[storeRecord]: NilFactorDatabase has no table_name set. You must extend this in your child class to use this function.");
        }
    }

    /**
     * @brief delete the record
     */
    public function deleteRecord() {
        $table_name = static::getTableName();
        $primary_key = static::$primary_key;
        if (!empty($table_name) && !empty($primary_key)) {
            if (!empty($this->{$primary_key})) {
                $sql = "delete from ".$table_name." where ".$primary_key." = :id";
                $params = ['id' => $this->{$primary_key}];
                static::query($sql, $params);
            } else {
                throw new Exception("ERROR[deleteRecord]: NilFactorDatabase primary_key empty no record to delete.");
            }
        } else {
            throw new Exception("ERROR[deleteRecord]: NilFactorDatabase has no table_name or primary_key set.");
        }
    }

    /**
     * @brief if there are creation_field or update_field we need to set these with the current time
     */
    protected function updateTimeFields() {
        $creation_field = static::$creation_field;
        $update_field = static::$update_field;
        if (!empty($creation_field) && empty($this->{$creation_field})) {
            $this->{$creation_field} = date("Y-m-d H:i:s");
        }
        if (!empty($update_field)) {
            $this->{$update_field} = date("Y-m-d H:i:s");
        }
    }

    /**
     * @brief prepare three arrays to allow us to generate the save sql
     * @param array $fields - the fields that are apart of this table
     * @return array
     */
    protected function getKeysAndValues(array $fields) {
        $keys = [];
        $sets = [];
        $values = [];
        $params = [];

        foreach($fields as $key => $type) {
            if ($key != static::$primary_key) {
                $value = $this->formatValue($type, $this->{$key});

                $params[$key] = $value;
                $keys[] = $key;
                $values[] = ":" .$key;
                $sets[] = $key ." = :" .$key;
            }
        }

        return [$keys, $sets, $values, $params];
    }

    /**
     * @brief format the given value for the type given
     * @param string $type - the type of field this is (num, date, string, bool, etc)
     * @param string $value - the given value for this field
     * @return string
     */
    protected function formatValue($type, $value) {
        if (empty($value)) {
            if (strtolower($type) == 'string') {
                $value = "";
            } else if (strtolower($type) == 'num') {
                $value = 0;
            } else if (strtolower($type) == 'bool') {
                $value = "f";
            } else {
                $value = "";
            }
        } else {
            if (strtolower($type) == 'timestamp') {
                $value = date("Y-m-d H:i:s", strtotime($value));
            } else if (strtolower($type) == 'timestamp_w_z') {
                $value = date("Y-m-d H:i:sO", strtotime($value));
            } else if (strtolower($type) == 'date') {
                $value = date("Y-m-d", strtotime($value));
            } else if (strtolower($type) == 'bool') {
                $value = !empty($value) ? "t" : "f";
            }
        }

        return $value;
    }
}
?>